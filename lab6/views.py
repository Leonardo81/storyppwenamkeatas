from django.shortcuts import render, redirect
from .forms import StatusForm
from .models import Status

def status(request):
    form = StatusForm()
    statuses = Status.objects.all()
    if request.method=="POST":
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('lab6:status')      
    return render(request, 'index.html', {'form':form, 'statuses':statuses})