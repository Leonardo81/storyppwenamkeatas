from django.urls import path, include
from . import views

app_name = 'lab6'

urlpatterns = [
    path('', views.status, name="status"),
]
