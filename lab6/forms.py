from . import models
from django import forms

class StatusForm(forms.ModelForm):
        status_message = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "input",
                "id" : "status-input",
                "required" : True,
                "placeholder":"What's your status",
                "max_length": 300,
                "autocomplete": 'off',
                }))

        class Meta:
                model = models.Status
                fields = ["status_message"]