from django.test import TestCase
from django.urls import resolve
from django.utils import timezone
from . import views
from .models import Status
from .forms import StatusForm
from django.http import HttpRequest

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium import webdriver

class UnitTest(TestCase):

        #test url '/' exist or not
    def test_url_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

        #test url using status view function
    def test_calling_right_views_function(self):
        found = resolve('/')
        self.assertEqual(found.func, views.status)

        #test '/' using templates index.html
    def test_using_right_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')

        #test response html contain "Status"
    def test_template_contain_status_word(self):
        request = HttpRequest()
        response = views.status(request)
        self.assertContains(response, 'Status')

        #test creating Status model and expecting right value of attribute
    def test_model_created(self):
        Status.objects.create(status_message = "PPW EASY AF")
        status_objs = Status.objects.all()
        self.assertEqual(len(status_objs), 1)
        
        the_obj = status_objs[0]
        self.assertEqual(the_obj.status_message, "PPW EASY AF")

        self.assertEqual(the_obj.waktu_pengisian.minute, timezone.now().minute)
        self.assertEqual(the_obj.waktu_pengisian.hour, timezone.now().hour)
        self.assertEqual(the_obj.waktu_pengisian.date(), timezone.now().date())

        #test get the model objects
    def test_get_model(self):
        status_obj = Status.objects.create(status_message = "PPW EASY AF")
        get_obj = Status.objects.get(status_message="PPW EASY AF")

        self.assertEqual(status_obj, get_obj)

        #test form creating and saving, and object created after saving form
    def test_form(self):
        form = StatusForm(data={'status_message':"PPW MANTAP"})

        self.assertTrue(form.is_valid())

        form.save()
        status_obj = Status.objects.get(id=1)
        self.assertNotEqual(status_obj, None)
        self.assertEqual(status_obj.status_message, form.cleaned_data['status_message'])

        #test the form are rendered in page
    def test_page_contained_form(self):
        response = self.client.get('/')
        self.assertContains(response, StatusForm()['status_message'])

        #test post request and saving the request data as Status
    def test_post_form(self):
        post_resp = self.client.post('/', data={'status_message':'MANTUL'})
        self.assertEqual(post_resp.status_code, 302)

        status = Status.objects.get(id=1)
        self.assertEqual(status.status_message, "MANTUL")

        #test input form and model with some number of charachter (max_length = 300 charachter)
    def test_input_maximum_length_300(self):
        long_text = ''
        for i in range(300):
            long_text += 'a'

        form = StatusForm(data={'status_message': long_text})
        self.assertEqual(len(form.data['status_message']), 300)

        post_resp = self.client.post('/', data={'status_message': long_text})
        status_obj = Status.objects.get(id=1)
        self.assertEqual(len(status_obj.status_message), 300)

        # try input more than 300 charachter
        long_text+='additional'
        form2 = StatusForm(data={'status_message': long_text})
        self.assertFalse(form2.is_valid())

        post_resp = self.client.post('/', data={'status_message': long_text})
        status_objs = Status.objects.all()
        self.assertEqual(len(status_objs), 1)

        #test the page will render all the Status model data
    def test_rendered_all_status_data(self):
        Status.objects.create(status_message="ASIKASIK")
        response = self.client.get('/')
        self.assertContains(response, 'ASIKASIK')

        for status in range(3):
            self.client.post('/', data={'status_message': 'status'+str(status)})

        response = self.client.get('/')
        for i in range(3):
            self.assertContains(response, 'status'+str(status))


class FunctionalTest(TestCase):

        #setup selenium browser
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

        #set tearing down
    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

        #test opening the web
    def test_open(self):
        self.browser.get('http://localhost:8000')
        self.assertEqual(self.browser.title, "PPW LEONARDO 6")
        self.assertIn("Status", self.browser.page_source)

        title = self.browser.find_element_by_id("title")
        self.assertEqual(title.text, "Share your Status Right now")

        #test submiting the form
    def test_send_status(self):
        self.browser.get('http://localhost:8000')
        status_input = self.browser.find_element_by_id("status-input")
        status_input.send_keys("LAGI GALAU")
        status_input.submit()
        status_input = self.browser.find_element_by_id("status-input")
        statuses = self.browser.find_element_by_name('statuses')
        self.assertIn("LAGI GALAU", self.browser.page_source)   #status in the page source
        self.assertNotIn("LAGI GALAU", status_input.get_attribute('value')) #status no longer in input box
        self.assertIn("LAGI GALAU", statuses.get_attribute('innerHTML'))    #status is in statuses data

        #test submit form with more than 300 words
    def test_send_more_than_300(self):
        self.browser.get('http://localhost:8000')

        long_text = ''
        for i in range(301):
            long_text += 'a'

        status_input = self.browser.find_element_by_id("status-input")
        status_input.send_keys(long_text)
        status_input.submit()

        status_input = self.browser.find_element_by_id("status-input")
        statuses = self.browser.find_element_by_name('statuses')

        self.assertIn(long_text, status_input.get_attribute('value')) #longtext still in input box, because post request failed
        self.assertNotIn(long_text, statuses.get_attribute('innerHTML'))    #no longtext in statuses data (not saved to models)
