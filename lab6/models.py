from django.db import models
from django.utils import timezone

class Status(models.Model):
    status_message = models.CharField(max_length=300)
    waktu_pengisian = models.DateTimeField(default=timezone.now)
