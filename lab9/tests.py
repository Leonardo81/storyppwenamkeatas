from django.test import TestCase
from django.urls import resolve
from django.utils import timezone
from . import views
from django.http import HttpRequest

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium import webdriver

class UnitTest(TestCase):

        #test url '/lab9' exist or not
    def test_url_exist(self):
        response = self.client.get('/lab9/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab9/login')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab9/logout')
        self.assertEqual(response.status_code, 302)

        #test url using status view function
    def test_calling_right_views_function(self):
        found = resolve('/lab9/')
        self.assertEqual(found.func, views.index)
        found = resolve('/lab9/login')
        self.assertEqual(found.func, views.login)
        found = resolve('/lab9/logout')
        self.assertEqual(found.func, views.logout)

        #test '/lab9/' using templates lab8.html
    def test_using_right_template(self):
        response = self.client.get('/lab9/')
        self.assertTemplateUsed(response, 'lab9.html')
        response = self.client.get('/lab9/login')
        self.assertTemplateUsed(response, 'login.html')

    def test_using_right_staticfiles(self):
        response = self.client.get('/lab9/')
        self.assertContains(response, 'static/css/style9')



class FunctionalTest(TestCase):

        #setup selenium browser
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

        #set tearing down
    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

        #test opening the web
    def test_open(self):
        self.browser.get('http://localhost:8000/lab9/')
        self.assertEqual(self.browser.title, "PPW LEONARDO 9")
        self.assertIn("Login", self.browser.page_source)

    def test_login(self):
        self.browser.get('http://localhost:8000/lab9/login')
        username = self.browser.find_element_by_id("id_username")
        username.send_keys("admin")
        password = self.browser.find_element_by_id("id_password")
        password.send_keys("admin")
        password.submit()
        self.assertIn("hello admin", self.browser.page_source)
    
