from django.test import TestCase
from django.urls import resolve
from django.utils import timezone
from . import views
from django.http import HttpRequest

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium import webdriver

class UnitTest(TestCase):

        #test url '/lab8' exist or not
    def test_url_exist(self):
        response = self.client.get('/lab8/')
        print()
        self.assertEqual(response.status_code, 200)

        #test url using status view function
    def test_calling_right_views_function(self):
        found = resolve('/lab8/')
        self.assertEqual(found.func, views.index)

        #test '/lab8/' using templates lab8.html
    def test_using_right_template(self):
        response = self.client.get('/lab8/')
        self.assertTemplateUsed(response, 'lab8.html')

        #test response html contain "Status"
    def test_template_contain_Activity_Experience_Achievement(self):
        request = HttpRequest()
        response = views.index(request)
        self.assertContains(response, 'Book')

    def test_using_right_staticfiles(self):
        response = self.client.get('/lab8/')
        self.assertContains(response, 'static/css/style8')
        self.assertContains(response, 'static/js/script8')


class FunctionalTest(TestCase):

        #setup selenium browser
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

        #set tearing down
    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

        #test opening the web
    def test_open(self):
        self.browser.get('http://localhost:8000/lab8/')
        self.assertEqual(self.browser.title, "PPW LEONARDO 8")
        self.assertIn("Book", self.browser.page_source)

    
